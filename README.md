# utilSublime

Utilities to use with Sublime Text 3.

## Highlighting of <tt>.prm</tt> files.
1. Copy the file [<tt>prm.sublime-syntax</tt>](https://gitlab.com/ifumagalli/utilsublime/blob/master/prm.sublime-syntax)
    in the User configuration path <br />
    (for Linux it should be `${HOME}/.config/sublime-text-3/Packages/User/`).
2. Open a <tt>.prm</tt> file.
3. If not done dy default, select `prm` in `View->Syntax`.